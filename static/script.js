const fileInput = document.querySelector('#file-input');
  const fileNameSpan = document.querySelector('#file-name');

  fileInput.addEventListener('change', () => {
    const file = fileInput.files[0];
    if (file) {
      fileNameSpan.textContent = file.name;
    } else {
      fileNameSpan.textContent = '';
    }
  });