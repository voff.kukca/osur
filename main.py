import os
from flask import Flask, send_from_directory, request, render_template
from werkzeug.utils import secure_filename

from check_file import check_file_type, check_file_size
from worker_logic.worker_lib import add_to_worker, delete_json
from osuref import get_json_data

app = Flask(__name__, static_folder='static')
app.config['UPLOAD_FOLDER'] = ''
REPLAY_VIDEO_FOLDER = ''

@app.route('/')
def index():
    return send_from_directory('static', 'index.html')

@app.route('/replays/<path:filename>')
def download_file(filename):
    return send_from_directory('replays', filename, as_attachment=True)

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

@app.route('/<int:worker_id>/queue', methods=['GET'])
def get_queue(worker_id):
    if worker_id == 1:
        file_path = '1.json'
        return send_from_directory('worker', file_path)
    else:
        return 'Worker not found', 404

@app.route('/process_replay', methods=['POST'])
def process_replay():
    # check if the post request has the file part
    print(request.files['replay'])
    if 'replay' not in request.files:
        return 'No file part', 400
    file = request.files['replay']
    if file.filename == '':
        return 'No selected file', 400
    # process the file
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], 'replays', file.filename))
    # generate random code
    js, code = get_json_data(file.filename)
    # return success response with generated code
    video_path = os.path.join(f'replays/{code}.mp4')
    # process replay
    add_to_worker(1, js)
    with open(os.path.join('codes', f'{code}'), 'w') as f:
        f.write(video_path)
    return render_template('processed.html', filename=code), 200

@app.route('/<filename>', methods=['GET'])
def download_code(filename):
    file_path = os.path.join('codes', filename)
    if not os.path.isfile(file_path):
        return '404'
    else:
        with open(file_path, 'r') as f:
            file_data = f.read()
        return render_template('file.html', filename=filename)

@app.route('/replay_video/<path:filename>')
def serve_video(filename):
    return send_from_directory('replay_video', filename)

@app.route('/<int:worker_id>/upload_video', methods=['POST'])
def upload_video(worker_id):
    if worker_id != 1:
        return 'Worker not found', 404
    if not os.path.exists('replay_video'):
        os.makedirs('replay_video')
    # получаем файл из запроса
    video_file = request.files.get('video')
    if not video_file:
        return 'Video file is missing', 400

    # сохраняем файл в папку replay_video
    video_path = os.path.join('replay_video', secure_filename(video_file.filename))
    video_file.save(video_path)

    # проверяем тип файла
    if not check_file_type(video_path):
        os.remove(video_path)  # удаляем файл, если тип не соответствует разрешенным типам
        return 'Invalid file type', 400

    # проверяем размер файла
    if not check_file_size(video_path):
        os.remove(video_path)  # удаляем файл, если его размер превышает максимальный допустимый размер
        return 'File size is too big', 400

    # отправляем ответ, что видео было принято
    delete_json(video_path, worker_id)
    return 'Video received', 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
