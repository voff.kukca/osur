# Базовый образ Python
FROM python:3.9-slim-buster

# Установка зависимостей из файла requirements.txt
COPY requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt

# Копирование приложения в контейнер
COPY . /app/

# Установка рабочей директории и переменных окружения
WORKDIR /app
ENV FLASK_APP=main.py
ENV FLASK_ENV=production

# Директива EXPOSE указывает на то, что контейнер ожидает соединения на порту 80
EXPOSE 80

# Запуск приложения на порту 80
CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]
