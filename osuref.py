import json
import secrets
import hashlib
import requests
import os.path

with open('secret.txt', 'r') as f:
    API_KEY = f.read().strip()
HASH = '<beatmap_hash>'
URL = 'http://0.0.0.0:5000/'

def extract_beatmap_id(json_str):
    json_data = json.loads(json_str)
    return json_data[0]['beatmap_id']

def find_hash_in_file(file_path):
    with open(file_path, 'rb') as f:
        rawdata = f.read()
    first_line = rawdata.decode('latin-1').split('\n')[0].strip()
    for i in range(len(first_line) - 31):
        if len(set(first_line[i:i+32]).difference(set('0123456789abcdefghijklmnopqrstuvwxyz'))) == 0:
            return first_line[i:i+32]
    return None

def get_download_path(map_id):
    url = f'https://api.chimu.moe/v1/map/{map_id}'
    response = requests.get(url)
    data = response.json()
    return data['DownloadPath']

def download_file(url, path):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"
    }
    response = requests.get(url, headers)
    filename = response.headers.get("Content-Disposition")
    if filename:
        filename = filename.split("filename=")[-1].strip('"')
    else:
        filename = "downloaded_file"

    with open(f"{path}/{filename}", "wb") as f:
        f.write(response.content)
    return filename

def get_json_data(file_path):
    code = secrets.token_hex(10)
    HASH = (find_hash_in_file(f'replays/{file_path}'))
    response = requests.get(f'https://osu.ppy.sh/api/get_beatmaps?k={API_KEY}&h={HASH}')
    json_str = response.text
    beatmap_id = extract_beatmap_id(json_str)
    download_path = get_download_path(beatmap_id)
    download_url = 'https://api.chimu.moe' + download_path
    # download_file(download_url, 'beatmap')
    code = hashlib.md5((download_url + f'{URL}replays/{file_path}').encode('utf-8')).hexdigest()
    d = {'beatmap': download_url, 'replay': f'{URL}replays/{file_path}', 'destination': code}
    return d, code

#print(get_json_data('replay-osu_1889804_4408557345(1).osr', '1234'))