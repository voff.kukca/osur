import os

ALLOWED_FILE_TYPES = ['.mp4', '.osr']
MAX_FILE_SIZE = 104857600  # 100 МБ


def check_file_type(file_path):
    ext = os.path.splitext(file_path)[1]
    return ext.lower() in ALLOWED_FILE_TYPES


def check_file_size(file_path):
    file_size = os.path.getsize(file_path)
    return file_size <= MAX_FILE_SIZE