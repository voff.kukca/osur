import json
import os

WORKER_FOLDER = "worker"
worker_files = {}

def get_data_file_path(worker_id):
    if worker_id in worker_files:
        return worker_files[worker_id]
    else:
        file_path = os.path.join(WORKER_FOLDER, f"{worker_id}.json")
        with open(file_path, "w") as f:
            json.dump([], f)
        worker_files[worker_id] = file_path
        return file_path
def add_to_worker(worker_id, json_data):
    data_file = get_data_file_path(worker_id)
    with open(data_file, "r") as f:
        queue_data = json.load(f)
    queue_data.append(json_data)
    with open(data_file, "w") as f:
        json.dump(queue_data, f)


def delete_json(video_path, worker_id):
    # Получаем id из названия видео
    video_id = os.path.splitext(os.path.basename(video_path))[0]

    # Открываем файл с данными обработчика
    worker_file_path = f'worker/{worker_id}.json'
    with open(worker_file_path, 'r') as f:
        worker_data = json.load(f)

    # Удаляем все записи с указанным destination
    worker_data = [d for d in worker_data if d.get('destination') != video_id]

    # Сохраняем измененные данные обработчика
    with open(worker_file_path, 'w') as f:
        json.dump(worker_data, f)