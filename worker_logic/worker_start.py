import requests
import shutil
import time
import os

video_folder = "video"
replay_file = "replay.mp4"
destination_folder = os.path.dirname(video_folder)

# Полный путь к файлу replay.mp4
replay_path = os.path.join(video_folder, replay_file)
# Полный путь к папке, находящейся выше
destination_path = os.path.abspath(os.path.join(video_folder, os.pardir))

while True:
    try:
        response = requests.get('http://0.0.0.0:5000/1/queue')
        if response.status_code == 200:
            queue = response.json()
            if queue:
                # Копируем файл replay.mp4 из папки video в папку выше
                shutil.copy(replay_path, os.path.join(destination_path, replay_file))
                data = queue.pop(0)
                # Обработка элемента очереди
                print(f"Processing data: {data['destination']}")
                src = 'replay.mp4'
                dst = str(data['destination'])+'.mp4'
                os.rename(src, dst)
                url = 'http://localhost:5000/1/upload_video'
                files = {'video': open(f'{dst}', 'rb')}
                response = requests.post(url, files=files)
                print(response.text)
                # Отправляем запрос на удаление элемента из очереди
            else:
                print("Queue is empty")
                time.sleep(30)
        else:
            print(f"Error getting queue: {response.text}")
        time.sleep(5) # ждем 5 секунд перед следующим запросом
    except Exception as e:
        print(f"Error: {e}")
        time.sleep(5) # ждем 5 секунд перед следующей попыткой